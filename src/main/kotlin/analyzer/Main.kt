package analyzer

fun main() {
    val view = ConsoleView(mutableListOf())
    view.init()
    view.start()
}