package analyzer

import analyzer.Users.name
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

object DBController {

    fun getUserByVKID(vk_id: String): User {
        val user = Users.select { Users.vk_id eq vk_id }.single()
        return User(user_id = user[Users.user_id], name = user[name], vk_id = user[Users.vk_id])
    }

    fun getGroupByID(vk_id: String): Group {
        val group = Groups.select { Groups.vk_id eq vk_id }.single()
        return Group(group_id = group[Groups.group_id], name = group[Groups.name], vk_id = group[Groups.vk_id])
    }

    fun getPostByIDs(vk_id: String, group_id: Int): Post {
        val post = Posts.select { (Posts.vk_id eq vk_id) and (Posts.group_id eq group_id) }.single()
        return Post(
            post_id = post[Posts.post_id],
            group_id = post[Posts.group_id],
            text = post[Posts.text],
            date = post[Posts.date],
            vk_id = post[Posts.vk_id],
            views = post[Posts.views]
        )
    }

    fun getGroupList(): List<Group> {
        return Groups.selectAll().map { group ->
            Group(group_id = group[Groups.group_id], name = group[Groups.name], vk_id = group[Groups.vk_id])
        }
    }

    fun addUser(user: User): Int {
        return Users insert user
    }

    fun addGroup(group: Group): Int {
        return Groups insert group
    }

    fun addPost(post: Post): Int {
        return Posts insert post
    }

    fun addLike(like: Like): Int {
        return Likes insert like
    }

    fun addComment(comment: Comment): Int {
        return Comments insert comment
    }
}
