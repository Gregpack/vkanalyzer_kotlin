package analyzer

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.vk.api.sdk.client.VkApiClient
import com.vk.api.sdk.client.actors.GroupActor
import com.vk.api.sdk.client.actors.ServiceActor
import com.vk.api.sdk.client.actors.UserActor
import com.vk.api.sdk.exceptions.ApiException
import com.vk.api.sdk.exceptions.ClientException
import com.vk.api.sdk.httpclient.HttpTransportClient
import com.vk.api.sdk.objects.users.UserXtrCounters
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import java.awt.Desktop
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import javax.swing.JOptionPane


@Throws(IOException::class, URISyntaxException::class)
fun askToken(): String? {
    val link =
        "https://oauth.vk.com/authorize?client_id=7254262&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=wall+groups&response_type=token&v=5.103"
    Desktop.getDesktop().browse(URI(link))
    println("Enter token from browser here:")
    return readLine()
}

class VKController(userId: Int, userToken: String = askToken()!!) {
    private val vk: VkApiClient = VkApiClient(HttpTransportClient.getInstance(), Gson(), 1)
    private val userActor: UserActor = UserActor(userId, userToken)

    @Throws(ClientException::class, ApiException::class, InterruptedException::class)
    private fun getOrCreateUser(vk_id: String): User {
        return try {
            DBController.getUserByVKID(vk_id)
        } catch (e: Exception) {
            createUser(vk_id)
        }
    }

    private fun isPostExists(vk_id: String, group_id: Int): Boolean {
        return try {
            DBController.getPostByIDs(vk_id, group_id)
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun getOrCreateGroup(vk_id: String): Group {
        return try {
            DBController.getGroupByID(vk_id)
        } catch (e: Exception) {
            createGroup(vk_id)
        }
    }

    private fun createGroup(vk_id: String): Group {
        val vkGroup = vk.groups().getById(userActor).groupId(vk_id).execute()[0]
        val newGroup = Group(name = vkGroup.name, vk_id = vkGroup.id.toString())
        val group_id = DBController.addGroup(newGroup)
        newGroup.group_id = group_id
        Thread.sleep(350)
        return newGroup
    }

    @Throws(ClientException::class, ApiException::class, InterruptedException::class)
    private fun createUser(vk_id: String): User {
        Thread.sleep(350)
        val name = if (vk_id.substring(0, 1) == "-") {
            vk.groups().getById(userActor).groupId(vk_id.substring(1)).execute()[0].name
        } else {
            val vkUser =
                vk.users()[userActor].userIds(vk_id).execute() as List<UserXtrCounters>
            vkUser[0].firstName + " " + vkUser[0].lastName
        }
        val dbUser = User(vk_id = vk_id, name = name)
        dbUser.user_id = DBController.addUser(dbUser)
        return dbUser
    }

    private fun userFromJson(vkUser: JsonElement) {
        val oVkUser = vkUser as JsonObject
        try {
            DBController.getUserByVKID(oVkUser["id"].asString)
        } catch (e: Exception) {
            val user = User(
                name = oVkUser["first_name"].asString + " " + oVkUser["last_name"].asString,
                vk_id = oVkUser["id"].asString
            )
            DBController.addUser(user)
        }
    }

    private fun commentFromJson(vkComment: JsonElement, post_id: Int) {
        val oVkComment = vkComment as JsonObject
        val user = getOrCreateUser(oVkComment["from_id"].asString)
        val commentText =
            if (oVkComment["text"].asString.length > 2500) oVkComment["text"].asString.substring(
                0,
                2500
            ) else oVkComment["text"].asString
        val likes = (oVkComment["likes"] as JsonObject)["count"].asInt
        val comment = Comment(user_id = user.user_id, post_id = post_id, text = commentText, likes = likes)
        DBController.addComment(comment)
    }

    private fun likeFromJson(vkLike: JsonElement, post_id: Int) {
        val vk_id = vkLike.asString
        val user = getOrCreateUser(vk_id)
        val like = Like(post_id = post_id, user_id = user.user_id)
        DBController.addLike(like)
    }

    private fun downloadUsers(groupName: String) {
        val userAmount = vk.execute().code(
            userActor,
            "var a = API.groups.getMembers({\"group_id\" : \"$groupName\"}); \n" +
                    "return a.count;"
        ).execute().asInt
        var i = 0
        while (i < userAmount) {
            val response = vk.execute().code(
                userActor,
                "var a = API.groups.getMembers({\"group_id\" : \"$groupName\", \"offset\" : $i});\n" +
                        "var b = API.users.get({\"user_ids\" : a.items});\n" +
                        "return b;"
            ).execute() as JsonArray
            response.forEach(this::userFromJson)
            i += 1000
        }

    }

    fun downloadGroup(name: String) {
        val group = getOrCreateGroup(name)
        downloadUsers(name)
        Thread.sleep(500)
        val postCount = if (name.toIntOrNull() != null) {
            vk.wall().get(userActor).ownerId("-$name".toInt()).execute().count
        } else {
            vk.wall().get(userActor).domain(name).execute().count
        }
        val param = if (name.toIntOrNull() != null) {
            "\"owner_id\" : \"-$name\""
        } else {
            "\"domain\" :  \"$name\""
        }
        var i = 0
        while (i < postCount) {
            Thread.sleep(500)
            val response = vk.execute().code(
                userActor,
                "var a = API.wall.get({$param, \"count\" : 10, \"offset\" : $i});\n" +
                        "        var i = 0;\n" +
                        "        var b = [];\n" +
                        "        var c = [];\n" +
                        "        while (i < a.items.length) {\n" +
                        "            b = b + [API.wall.getComments({\"owner_id\" : a.items[0].owner_id, \"post_id\" : a.items[i].id, \"count\" : 100, \"need_likes\" : 1})];\n" +
                        "            c = c + [API.likes.getList({\"type\" : \"post\", \"owner_id\" : a.items[0].owner_id, \"item_id\" : a.items[i].id, \"count\" : 1000})];\n" +
                        "            i = i + 1;\n" +
                        "        }\n" +
                        "        return [a, b, c];"
            ).execute() as JsonArray
            val jsonPosts = response[0].asJsonObject["items"] as JsonArray
            val jsonComments = response[1].asJsonArray
            val jsonLikes = response[2].asJsonArray
            jsonPosts.forEachIndexed { counter, vkPost ->
                val oVkPost = vkPost.asJsonObject
                val vkId = oVkPost["id"].asString
                if (isPostExists(vkId, group.group_id))
                    return
                val postViews = oVkPost["views"].asJsonObject["count"].asInt
                val postDate = DateTime(1970, 1, 1, 0, 0).plus(oVkPost["date"].asInt.toLong() * 100)
                val postText = if (oVkPost["text"].asString.length > 2500) oVkPost["text"].asString.substring(
                    0,
                    2500
                ) else oVkPost["text"].asString
                val post =
                    Post(views = postViews, date = postDate, text = postText, group_id = group.group_id, vk_id = vkId)
                val post_id = DBController.addPost(post)
                val comments = (jsonComments[counter] as JsonObject)["items"] as JsonArray
                comments.forEach { vkComment -> commentFromJson(vkComment, post_id) }
                val likes = (jsonLikes[counter] as JsonObject)["items"] as JsonArray
                likes.forEach { vkLike -> likeFromJson(vkLike, post_id) }
            }
            i += 10
        }
    }

}
