package analyzer

import analyzer.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.jodatime.date
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

object Users : Table() {
    val user_id = integer("user_id").autoIncrement()
    val name = varchar("name", length = 100)
    val vk_id = varchar("vk_id", length = 100)
    override val primaryKey = PrimaryKey(user_id, name = "users_pkey")
    public infix fun insert(user: User) : Int{
        return Users.insert{
            it[name] = user.name
            it[vk_id] = user.vk_id
        }[user_id]
    }
}

open class User(var user_id: Int = 0, val name: String, val vk_id: String)

object Groups : Table() {
    val group_id = integer("group_id").autoIncrement()
    val name = varchar("name", length = 100)
    val vk_id = varchar("vk_id", length = 50)
    override val primaryKey = PrimaryKey(group_id, name = "groups_pkey")
    public infix fun insert(group: Group) : Int{
        return Groups.insert{
            it[name] = group.name
            it[vk_id] = group.vk_id
        }[group_id]
    }
}

open class Group(var group_id: Int = 0, val name: String, val vk_id: String)

object Posts : Table() {
    val post_id = integer("post_id").autoIncrement()
    val views = integer("views")
    val date = date("date")
    val text = varchar("text", 2500)
    val vk_id = varchar("vk_id", 50)
    val group_id = integer("group_id") references Groups.group_id
    override val primaryKey = PrimaryKey(post_id, name = "posts_pkey")
    public infix fun insert(post : Post) : Int{
        return Posts.insert{
            it[views] = post.views
            it[date] = post.date
            it[text] = post.text
            it[group_id] = post.group_id
            it[vk_id] = post.vk_id
        }[post_id]
    }
}

open class Post(val post_id: Int = 0, val views: Int, val date: DateTime, val text: String, val vk_id : String, val group_id: Int)

object Likes : Table() {
    val like_id = integer("like_id").autoIncrement()
    val post_id = integer("post_id") references Posts.post_id
    val user_id = integer("user_id") references Users.user_id
    override val primaryKey = PrimaryKey(like_id, name = "likes_pkey")
    public infix fun insert(like : Like) : Int{
        return Likes.insert{
            it[user_id] = like.user_id
            it[post_id] = like.post_id
        }[like_id]
    }
}

open class Like(val like_id: Int = 0, val post_id: Int, val user_id: Int)

object Comments : Table() {
    val comment_id = integer("comment_id").autoIncrement()
    val user_id = integer("user_id") references Users.user_id
    val post_id = integer("post_id") references Posts.post_id
    val likes = integer("likes")
    val text = varchar("text", 2500)
    override val primaryKey = PrimaryKey(comment_id, name = "comments_pkey")

    public infix fun insert(comment : Comment) : Int{
        return Comments.insert{
            it[user_id] = comment.user_id
            it[post_id] = comment.post_id
            it[likes] = comment.likes
            it[text] = comment.text
        }[comment_id]
    }
}

open class Comment(val comment_id: Int = 0, val user_id: Int, val post_id: Int, val likes: Int, val text: String)
