package analyzer

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.lang.UnsupportedOperationException
import kotlin.system.exitProcess

enum class MenuLevel {
    MAIN_MENU, ANALYZE_LIST, GROUP_DOWNLOAD, GROUP_LIST
}

class Analyze(val name: String, val factoryName: String, val desc: String)

class NoSuchOptionException : Exception()


class ConsoleView(val analyzes: MutableList<Analyze>, var menuLevel: MenuLevel = MenuLevel.MAIN_MENU) {
    fun printOptions() = when (menuLevel) {
        MenuLevel.MAIN_MENU -> println("1. Скачать группу\n2. Провести анализ\n3. Список скачанных групп")
        MenuLevel.ANALYZE_LIST -> {
            analyzes.forEachIndexed { i, it -> println("${i + 1}. ${it.name}") }
            println("${analyzes.size + 1}. Назад")
        }
        MenuLevel.GROUP_DOWNLOAD -> {
            println("Введите ваш id и имя или id группы. Примеры: 52024127 160254263, 52024127 gaybar220 1.\nЧтобы вернуться назад, введите -1.")
        }
        MenuLevel.GROUP_LIST -> {
            transaction {
                SchemaUtils.create(Groups)
                DBController.getGroupList().forEach { group -> println("${group.name} - vk_id: ${group.vk_id}") }
            }
        }
    }

    fun executeOption() {
        when (menuLevel) {
            MenuLevel.MAIN_MENU -> {
                val number = readLine()!!.toInt()
                when (number) {
                    1 -> {
                        this.menuLevel = MenuLevel.GROUP_DOWNLOAD
                    }
                    2 -> {
                        this.menuLevel = MenuLevel.ANALYZE_LIST
                    }
                    3 -> {
                        this.menuLevel = MenuLevel.GROUP_LIST
                    }
                    else -> throw NoSuchOptionException()
                }
            }
            MenuLevel.ANALYZE_LIST -> {
                val number = readLine()!!.toInt()
                if (number == analyzes.size + 1) {
                    menuLevel = MenuLevel.MAIN_MENU
                } else {
                    if (number > analyzes.size + 1) {
                        throw NoSuchOptionException()
                    }
                    val an = analyzes[number - 1]
                    println(an.desc)
                    val args = readLine()!!.split(" ")
                    try {
                        AnalyzeController.analyzeByName(an.factoryName).call(*args.toTypedArray())
                    } catch (e: IllegalArgumentException) {
                        println("Вы ввели неверное число аргументов.")
                        menuLevel = MenuLevel.MAIN_MENU
                    }
                }
            }
            MenuLevel.GROUP_DOWNLOAD -> {
                val args = readLine()!!.split(" ")
                if (args[0].toIntOrNull() == -1) {
                    menuLevel = MenuLevel.MAIN_MENU
                    return
                }
                if (args.size != 2) {
                    println("Вы ввели неверное число аргументов, либо неверные аргументы.")
                    menuLevel = MenuLevel.MAIN_MENU
                    return
                }
                val vkController = VKController(args[0].toInt())
                transaction {
                    addLogger(StdOutSqlLogger)
                    SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
                    try {
                        vkController.downloadGroup(args[1])
                    } catch (e: UnsupportedOperationException) {
                        println("Произошла ошибка при скачке группы. Возможно, вы написали не свой vk id или указали несуществующую группу?")
                        menuLevel = MenuLevel.MAIN_MENU
                    }
                }
            }
            MenuLevel.GROUP_LIST -> menuLevel = MenuLevel.MAIN_MENU
        }
    }

    fun init() {
        try {
            File("analyzes.alz").readLines().forEach {
                val line = it.split("/")
                analyzes.add(Analyze(factoryName = line[0], name = line[1], desc = line[2]))
            }
        } catch (e: Exception) {
            println("Ошибка при парсинге файла анализов. Проверьте, возможно, вы его оформили не в соответствии с шаблоном?")
            exitProcess(0)
        }
    }

    fun start() {
        try {
            val dbConfig = File("db.config").readLines().map {
                val line = it.split("=")
                Pair(line[0], line[1])
            }.toMap()
            Database.connect(
                url = dbConfig["url"]!!,
                driver = "org.postgresql.Driver",
                user = dbConfig["user"]!!,
                password = dbConfig["password"]!!
            )
        } catch (e: Exception) {
            println("Ошибка при подключении к БД. Возможно, у вас неверно заполнен конфиг?")
            exitProcess(0)
        }
        while (true) {
            printOptions()
            try {
                executeOption()
            } catch (e: NumberFormatException) {
                println("Вы ввели не число.")
            } catch (e: NoSuchOptionException) {
                println("Вы ввели число, не соответствующее ни одной опции.")
            } catch (e: NoSuchAnalyzeException) {
                println("Указанный анализ не найден. Проверьте analyzes.alz.")
            }
        }
    }
}
