import kotlin.math.pow
import kotlin.math.sqrt

fun stdev(numArray: FloatArray): Float {
    var sum = 0.0f
    var standardDeviation = 0.0f
    for (num in numArray) {
        sum += num
    }
    val mean = sum / numArray.size
    for (num in numArray) {
        standardDeviation += (num - mean).pow(2.0f)
    }
    return sqrt(standardDeviation / numArray.size).toFloat()
}

fun mavg(list : List<Float>) : Float {
    val sum = list.reduce{acc, it -> acc + it}
    return sum / list.size
}

fun createPostLink(group_id : String, vk_id : String) : String {
    return "https://vk.com/club${group_id}?w=wall-${group_id}_${vk_id}"
}

fun createUserLink(vk_id : String) : String {
    return if (vk_id[0] == '-') {
        "https://vk.com/club${vk_id.substring(1)}"
    } else {
        "https://vk.com/id${vk_id}"
    }
}

fun cutLongText(text: String) : String {
    return if (text.length > 40) {
        text.take(40) + "..."
    } else {
        text
    }
}
