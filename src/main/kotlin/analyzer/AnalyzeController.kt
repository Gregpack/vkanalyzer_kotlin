package analyzer

import createPostLink
import createUserLink
import cutLongText
import mavg
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import stdev
import kotlin.math.abs
import kotlin.math.sqrt
import kotlin.reflect.KFunction

class NoSuchAnalyzeException : Exception()

object AnalyzeController {
    fun analyzeByName(name: String): KFunction<*> {
        return when (name) {
            "uTopLikes" -> this::topNUsersByLikes
            "uTopComments" -> this::topNUsersByComments
            "bestCommenters" -> this::bestCommenters
            "pTopLikes" -> this::topNPostsByLikes
            "pTopComments" -> this::topNPostsByComments
            "likesPerView" -> this::topNPostsByLikePerView
            "commentsPerView" -> this::topNPostsByCommentPerView
            "searchComments" -> this::doCommentFTSSearch
            "searchPosts" -> this::doPostFTSSearch
            else -> throw NoSuchAnalyzeException()
        }
    }

    fun topNUsersByLikes(vk_id: String, N: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val query = ((Users innerJoin Likes) innerJoin Posts innerJoin Groups)
                .slice(Users.user_id, Users.vk_id, Users.name, Users.user_id.count())
                .select {
                    (Users.user_id eq Likes.user_id)
                        .and(Likes.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Users.user_id)
                .orderBy(Users.user_id.count(), SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it -> println("${i + 1}. ${createUserLink(it[Users.vk_id])}, ${it[Users.name]} : ${it[Users.user_id.count()]}") }
        }
    }

    fun topNUsersByComments(vk_id: String, N: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)

            ((Users innerJoin Comments) innerJoin Posts innerJoin Groups)
                .slice(Users.user_id, Users.vk_id, Users.name, Users.user_id.count())
                .select {
                    (Users.user_id eq Comments.user_id)
                        .and(Comments.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Users.user_id)
                .orderBy(Users.user_id.count(), SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it -> println("${i + 1}. ${createUserLink(it[Users.vk_id])}, ${it[Users.name]} : ${it[Users.user_id.count()]}") }
        }
    }

    fun bestCommenters(vk_id: String, N: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val matstatQuery = Users.innerJoin(Comments.innerJoin(Posts.innerJoin(Groups)))
                .slice(Comments.likes.sum(), Users.user_id.count())
                .select {
                    (Groups.group_id eq Posts.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                        .and(Posts.post_id eq Comments.post_id)
                        .and(Comments.user_id eq Users.user_id)
                }.groupBy(Users.user_id)
                .orderBy(Comments.likes.sum(), SortOrder.DESC)
                .map { it[Comments.likes.sum()]!!.toFloat() / sqrt(it[Users.user_id.count()].toDouble()).toFloat() }
            val stddev = stdev(matstatQuery.toFloatArray())
            val avg = mavg(matstatQuery)
            val ansQuery = Users.innerJoin(Comments.innerJoin(Posts.innerJoin(Groups)))
                .slice(Users.name, Users.vk_id, Comments.likes.sum(), Users.user_id.count())
                .select {
                    (Groups.group_id eq Posts.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                        .and(Posts.post_id eq Comments.post_id)
                        .and(Comments.user_id eq Users.user_id)
                }.groupBy(Users.user_id)
                .map {
                    Triple(
                        it[Users.name],
                        abs((it[Comments.likes.sum()]!!.toFloat() / sqrt(it[Users.user_id.count()].toFloat())) - avg) / stddev,
                        it[Users.vk_id]
                    )
                }
                .sortedByDescending { it.second }
                .subList(0, N.toInt())
                .forEachIndexed { i, it -> println("${i + 1}. ${createUserLink(it.third)}, ${it.first}") }
        }
    }

    fun topNPostsByLikes(vk_id: String, N: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val query = (Likes innerJoin Posts innerJoin Groups)
                .slice(Posts.post_id, Posts.vk_id, Posts.text, Posts.post_id.count())
                .select {
                    (Likes.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Posts.post_id)
                .orderBy(Posts.post_id.count(), SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it ->
                    println(
                        "${i + 1}. Пост: ${createPostLink(
                            vk_id,
                            it[Posts.vk_id]
                        )} Лайков: ${it[Posts.post_id.count()]}"
                    )
                }
        }
    }

    fun topNPostsByComments(vk_id: String, N: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val query = (Comments innerJoin Posts innerJoin Groups)
                .slice(Posts.post_id, Posts.vk_id, Posts.text, Posts.post_id.count())
                .select {
                    (Comments.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Posts.post_id)
                .orderBy(Posts.post_id.count(), SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it ->
                    println(
                        "${i + 1}. Пост: ${createPostLink(
                            vk_id,
                            it[Posts.vk_id]
                        )}, комментариев: ${it[Posts.post_id.count()]}"
                    )
                }
        }
    }

    fun topNPostsByLikePerView(vk_id: String, N: String) {
        val expr = Expression.build {
            Posts.post_id.count().castTo<Float>(FloatColumnType()) / Posts.views.castTo<Float>(FloatColumnType())
        }
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val query = (Likes innerJoin Posts innerJoin Groups)
                .slice(Posts.views, Posts.post_id, Posts.vk_id, Posts.text, Posts.post_id.count(), expr)
                .select {
                    (Likes.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Posts.post_id)
                .orderBy(expr, SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it ->
                    println(
                        "${i + 1}. Пост: ${createPostLink(
                            vk_id,
                            it[Posts.vk_id]
                        )}, просмотров: ${it[Posts.views]}, лайков: ${it[Posts.post_id.count()]}, коэффицент: ${it[expr]}"
                    )
                }
        }
    }

    fun topNPostsByCommentPerView(vk_id: String, N: String) {
        val expr = Expression.build {
            Posts.post_id.count().castTo<Float>(FloatColumnType()) / Posts.views.castTo<Float>(FloatColumnType())
        }
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            val group = DBController.getGroupByID(vk_id)
            val query = (Comments innerJoin Posts innerJoin Groups)
                .slice(Posts.views, Posts.post_id, Posts.vk_id, Posts.text, Posts.post_id.count(), expr)
                .select {
                    (Comments.post_id eq Posts.post_id)
                        .and(Posts.group_id eq Groups.group_id)
                        .and(Groups.vk_id eq group.vk_id)
                }
                .groupBy(Posts.post_id)
                .orderBy(expr, SortOrder.DESC)
                .limit(N.toInt())
                .forEachIndexed { i, it ->
                    println(
                        "${i + 1}. Пост: ${createPostLink(
                            vk_id,
                            it[Posts.vk_id]
                        )}, просмотров: ${it[Posts.views]}, комментов: ${it[Posts.post_id.count()]}, коэффицент: ${it[expr]}"
                    )
                }
        }
    }

    private fun createFTSIndices() {
        TransactionManager.current()
            .exec("CREATE INDEX IF NOT EXISTS fts_posts ON posts USING GIN (to_tsvector('english', text));")
        TransactionManager.current()
            .exec("CREATE INDEX IF NOT EXISTS fts_comments ON comments USING GIN (to_tsvector('english', text));")
    }

    fun doPostFTSSearch(vk_id: String, searchFor: String, limit: String) {
        transaction {
            //addLogger(StdOutSqlLogger)
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            createFTSIndices()
            val query = searchFor.replace(",", " | ").replace(".", " & ")
            val search = TransactionManager.current()
                .exec(
                    """SELECT posts.post_id, posts.vk_id, posts.text 
                        FROM posts JOIN groups ON posts.group_id = groups.group_id, 
                        to_tsquery('${query}') AS query
                        WHERE to_tsvector('russian', text) @@ query AND groups.vk_id = '${vk_id}'
                        LIMIT ${limit};
                        """
                ) { rs ->
                    val result = mutableListOf<Pair<String, String>>()
                    while (rs.next()) {
                        result += Pair(rs.getString("vk_id"), rs.getString("text"))
                    }
                    result
                }!!.forEachIndexed { index, it ->
                println("${index}) ${cutLongText(it.second)} ${createPostLink(vk_id, it.first)}")
            }
        }
    }

    fun doCommentFTSSearch(vk_id: String, searchFor: String, limit: String) {

        transaction {
            SchemaUtils.create(Users, Posts, Groups, Likes, Comments)
            createFTSIndices()
            val query = searchFor.replace(",", " | ").replace(".", " & ")
            val search = TransactionManager.current()
                .exec(
                    """SELECT posts.vk_id, comments.text
                            FROM comments 
                            JOIN posts ON comments.post_id = posts.post_id
                            JOIN groups ON posts.group_id = groups.group_id, 
                            to_tsquery('${query}') AS query
                            WHERE to_tsvector('russian', comments.text) @@ query AND groups.vk_id = '${vk_id}'
                            LIMIT ${limit};
                        """
                ) { rs ->
                    val result = mutableListOf<Pair<String, String>>()
                    while (rs.next()) {
                        result += Pair(rs.getString("vk_id"), rs.getString("text"))
                    }
                    result
                }!!.forEachIndexed { index, it ->
                println("${index}) ${cutLongText(it.second)} ${createPostLink(vk_id, it.first)}")
            }
        }
    }
}
